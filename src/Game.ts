///<reference path = "../tsDefinitions/phaser.d.ts" />

module codeTemplate {
	
	export var music:Phaser.Sound = null;
	export var orientated:boolean = false;
	export var buttonPressed:boolean = false;
	
	export class Game extends Phaser.Game {
		
		
		constructor(){
			
			super(1024, 768, Phaser.AUTO, 'content', null);
			
			this.state.add('Boot', Boot, false);
			this.state.add('Preloader', Preloader, false);
			this.state.add('Sim', Sim, false);
			
			
			this.state.start('Boot');
			
			
		}
		
	}
	
}
