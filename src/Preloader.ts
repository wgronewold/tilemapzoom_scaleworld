///<reference path = "../tsDefinitions/phaser.d.ts" />
module codeTemplate {
	
	export class Preloader extends Phaser.State{
		
		background: Phaser.Color;
		ready: boolean;
		
		preload(){
			
			//this.game.time.advancedTiming = true;
			//this.load.image/spritesheet/tilemap
			this.load.image('tiles', 'assets/tileset.png');
			
		}
		
		create(){
			
			
			
		}
		
		update(){
			
			this.game.state.start('Sim');
			
			
		}
		
		
	}
	
	
	
}