///<reference path = "../tsDefinitions/phaser.d.ts" />
module codeTemplate {
	
	export class Boot extends Phaser.State {
		
		init()
                {
                        
                        this.input.maxPointers = 1;
                        this.stage.disableVisibilityChange = true;
			
		      if (this.game.device.desktop)
                        {
                            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                            this.scale.setMinMax(480, 260, 1024, 768);
                            this.scale.pageAlignHorizontally = true;
                            this.scale.pageAlignVertically = true;
                        }
                        else
                        {
                            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                            this.scale.setMinMax(480, 260, 1024, 768);
                            this.scale.pageAlignHorizontally = true;
                            this.scale.pageAlignVertically = true;
                            this.scale.forceOrientation(true, false);
                            this.scale.setResizeCallback(this.gameResized, this);
                            this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
                            this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
                        }
			
			
		}
		
		preload() {
			
			//this.preload.image('logo', 'assets/ds_logo.png');
			//this.preload.image
			//this.preload.image
			
		}
		
		create() {
			
			//this.input.maxPoin
			this.game.state.start('Preloader');
			
			
		}
            
            gameResized(){
                  
                  
                  
                  
                  
            }
            
            enterIncorrectOrientation() {
                  
                  
                  codeTemplate.orientated = false;
                  document.getElementById('orientation').style.display = 'block';
                        
            }
            
            leaveIncorrectOrientation(){
                  
                  codeTemplate.orientated = true;
                  
                  document.getElementById('orientation').style.display = 'none';
                  
                  
            }
		
		
	}
	
	
}