///<reference path = "../tsDefinitions/phaser.d.ts" />
module codeTemplate {
	
	export class Sim extends Phaser.State{
		
		map: Phaser.Tilemap;
		layer: Phaser.TilemapLayer;
		currentTile: Phaser.Tile;
		
		marker: Phaser.Graphics;
		cursors: any;
		layerScale: number;
		inputScale: number;
		
		create(){
			
			this.stage.backgroundColor = "#4DBD33";
			
			// Create the tilemap and the tilelayer
			this.map = this.add.tilemap();
			this.map.addTilesetImage('tiles');
			this.map.setTileSize(32, 32);
			
			
			// Camera Size is 1024x768
			// For this prototype/test I would like the world to be 3 times larger
			// than the camera
			
			// Sets world width to 3072 
			// 96 Tiles * Tile width(32 pixels)
			// World Height = 2304
			// 72 Tiles * Tile height(32 pixels)
			
			this.layer = this.map.create('layer', 96, 72, 32, 32);
			this.layer.resizeWorld();
			
			// This holds a Tile from the tilemap's tileset
			// Will be used to "paint" the tilemap
			this.currentTile = new Phaser.Tile(this.layer, 5, this.input.activePointer.x, 
				this.input.activePointer.y, 32, 32 );
			
			this.marker = this.add.graphics(0, 0);
			this.marker.lineStyle(2, 0x000000, 1);
			this.marker.drawRect(0, 0, 32, 32);
			
			// Input
			this.input.addMoveCallback(this.updateMarker, this);
			this.cursors = this.input.keyboard.createCursorKeys();
			
			// I believe input scale needs to be an inverse of the world
			// This works fairly accurate when world scale is set between .33 and 1
			// And inputscale is between 1 and 1.66
			// Doesn't work to well when the scales are inbetween these numbers
			  
			  
			// I would like  
			this.layerScale = .33;
			this.inputScale = 3;
			
		}
		
		updateMarker(){
			
			
			// Draw a Tile Marker and update its position
			this.marker.x = this.layer.getTileX(this.input.activePointer.worldX) * 32;
			this.marker.y = this.layer.getTileY(this.input.activePointer.worldY) * 32;
			
			if(this.input.mousePointer.isDown){
				
				this.map.putTile(this.currentTile, this.layer.getTileX(this.marker.x), 
					this.layer.getTileY(this.marker.y), this.layer);
					
				// console.log(this.layer.getTileX(this.marker.x));
				// console.log(this.layer.getTileY(this.marker.x));
				
			}
			
			
		}
		
		update() {
			
			// Handle scaling
			// Not exactly perfect for aspect ration of the camera atm
			if(this.input.keyboard.isDown(Phaser.Keyboard.Q)){
				
				this.layerScale += 0.02;
				this.inputScale -= 0.02;

				
				
			}
			if(this.input.keyboard.isDown(Phaser.Keyboard.A)){
				
				this.layerScale -= 0.02;
				this.inputScale += 0.02;
				
			}
			
			this.layerScale = Phaser.Math.clamp(this.layerScale, .33, 1);
			this.inputScale = Phaser.Math.clamp(this.inputScale, 1, 3);
			
			this.layer.scale.set(this.layerScale);
			this.input.scale.set(this.inputScale);
			
		
		}
		
		
		render() {
			
			//this.game.debug.cameraInfo(this.camera, 16, 16);
			this.game.debug.inputInfo( 16, 100);
			this.game.debug.text("Layer Scale X: " + this.layer.scale.x, 16, 16);
			this.game.debug.text("Layer Scale Y: " + this.layer.scale.y, 16, 32);
			
			
		}
		
	}
	
	
}