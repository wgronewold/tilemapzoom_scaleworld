var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path = "../tsDefinitions/phaser.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.init = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            if (this.game.device.desktop) {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.scale.setMinMax(480, 260, 1024, 768);
                this.scale.pageAlignHorizontally = true;
                this.scale.pageAlignVertically = true;
            }
            else {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.scale.setMinMax(480, 260, 1024, 768);
                this.scale.pageAlignHorizontally = true;
                this.scale.pageAlignVertically = true;
                this.scale.forceOrientation(true, false);
                this.scale.setResizeCallback(this.gameResized, this);
                this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
                this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
            }
        };
        Boot.prototype.preload = function () {
            //this.preload.image('logo', 'assets/ds_logo.png');
            //this.preload.image
            //this.preload.image
        };
        Boot.prototype.create = function () {
            //this.input.maxPoin
            this.game.state.start('Preloader');
        };
        Boot.prototype.gameResized = function () {
        };
        Boot.prototype.enterIncorrectOrientation = function () {
            codeTemplate.orientated = false;
            document.getElementById('orientation').style.display = 'block';
        };
        Boot.prototype.leaveIncorrectOrientation = function () {
            codeTemplate.orientated = true;
            document.getElementById('orientation').style.display = 'none';
        };
        return Boot;
    })(Phaser.State);
    codeTemplate.Boot = Boot;
})(codeTemplate || (codeTemplate = {}));
///<reference path = "../tsDefinitions/phaser.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    codeTemplate.music = null;
    codeTemplate.orientated = false;
    codeTemplate.buttonPressed = false;
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 1024, 768, Phaser.AUTO, 'content', null);
            this.state.add('Boot', codeTemplate.Boot, false);
            this.state.add('Preloader', codeTemplate.Preloader, false);
            this.state.add('Sim', codeTemplate.Sim, false);
            this.state.start('Boot');
        }
        return Game;
    })(Phaser.Game);
    codeTemplate.Game = Game;
})(codeTemplate || (codeTemplate = {}));
///<reference path = "../tsDefinitions/phaser.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            //this.game.time.advancedTiming = true;
            //this.load.image/spritesheet/tilemap
            this.load.image('tiles', 'assets/tileset.png');
        };
        Preloader.prototype.create = function () {
        };
        Preloader.prototype.update = function () {
            this.game.state.start('Sim');
        };
        return Preloader;
    })(Phaser.State);
    codeTemplate.Preloader = Preloader;
})(codeTemplate || (codeTemplate = {}));
///<reference path = "../tsDefinitions/phaser.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    var Sim = (function (_super) {
        __extends(Sim, _super);
        function Sim() {
            _super.apply(this, arguments);
        }
        Sim.prototype.create = function () {
            this.stage.backgroundColor = "#4DBD33";
            // Create the tilemap and the tilelayer
            this.map = this.add.tilemap();
            this.map.addTilesetImage('tiles');
            this.map.setTileSize(32, 32);
            // Camera Size is 1024x768
            // For this prototype/test I would like the world to be 3 times larger
            // than the camera
            // Sets world width to 3072 
            // 96 Tiles * Tile width(32 pixels)
            // World Height = 2304
            // 72 Tiles * Tile height(32 pixels)
            this.layer = this.map.create('layer', 96, 72, 32, 32);
            this.layer.resizeWorld();
            // This holds a Tile from the tilemap's tileset
            // Will be used to "paint" the tilemap
            this.currentTile = new Phaser.Tile(this.layer, 5, this.input.activePointer.x, this.input.activePointer.y, 32, 32);
            this.marker = this.add.graphics(0, 0);
            this.marker.lineStyle(2, 0x000000, 1);
            this.marker.drawRect(0, 0, 32, 32);
            // Input
            this.input.addMoveCallback(this.updateMarker, this);
            this.cursors = this.input.keyboard.createCursorKeys();
            // I believe input scale needs to be an inverse of the world
            // This works fairly accurate when world scale is set between .33 and 1
            // And inputscale is between 1 and 1.66
            // Doesn't work to well when the scales are inbetween these numbers
            // I would like  
            this.worldScale = .33;
            this.inputScale = 3;
        };
        Sim.prototype.updateMarker = function () {
            // Draw a Tile Marker and update its position
            this.marker.x = this.layer.getTileX(this.input.activePointer.worldX) * 32;
            this.marker.y = this.layer.getTileY(this.input.activePointer.worldY) * 32;
            if (this.input.mousePointer.isDown) {
                this.map.putTile(this.currentTile, this.layer.getTileX(this.marker.x), this.layer.getTileY(this.marker.y), this.layer);
            }
        };
        Sim.prototype.update = function () {
            // Handle scaling
            // Not exactly perfect for aspect ration of the camera atm
            if (this.input.keyboard.isDown(Phaser.Keyboard.Q)) {
                this.worldScale += 0.02;
                this.inputScale -= 0.02;
            }
            if (this.input.keyboard.isDown(Phaser.Keyboard.A)) {
                this.worldScale -= 0.02;
                this.inputScale += 0.02;
            }
            this.worldScale = Phaser.Math.clamp(this.worldScale, .33, 1);
            this.inputScale = Phaser.Math.clamp(this.inputScale, 1, 3);
            this.world.scale.set(this.worldScale);
            this.input.scale.set(this.inputScale);
            if (this.cursors.left.isDown) {
                this.camera.x -= 20;
            }
            else if (this.cursors.right.isDown) {
                this.camera.x += 20;
            }
            if (this.cursors.up.isDown) {
                this.camera.y -= 20;
            }
            else if (this.cursors.down.isDown) {
                this.camera.y += 20;
            }
        };
        Sim.prototype.render = function () {
            //this.game.debug.cameraInfo(this.camera, 16, 16);
            this.game.debug.inputInfo(16, 100);
            this.game.debug.text("World Scale X: " + this.world.scale.x, 16, 16);
            this.game.debug.text("World Scale Y: " + this.world.scale.y, 16, 32);
        };
        return Sim;
    })(Phaser.State);
    codeTemplate.Sim = Sim;
})(codeTemplate || (codeTemplate = {}));
window.onload = function () {
    var game = new codeTemplate.Game();
};
